//
//  Card.swift
//  ManCards
//
//  Created by MacMini on 10/05/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class Card: NSObject {
    var durationDisplayString: String?{
        get {
            return "Recommended duration: " + (self.duration?.stringValue)! + " days"
        }
    }
    var category: String?
    var cardDescription: String?
    var duration: NSNumber?
    var id: NSNumber?
    var title: String?
    var excuse: String?
    var startDate: String?
    
    func dictionaryValue() -> [AnyHashable : Any] {
        return ["category" : self.category!, "cardDescription" : self.cardDescription!, "duration" : self.duration!, "id" : self.id!, "title" : self.title!]
    }
    
    convenience init(jsonData: [String : AnyObject]) {
        self.init()
        
        if let category = jsonData["category"] {
            self.category = category as? String
        }
        
        if let cardDescription = jsonData["cardDescription"] {
            self.cardDescription = cardDescription as? String
        }
        
        if let duration = jsonData["duration"] {
            self.duration = duration as? NSNumber
        }
        
        if let id = jsonData["id"] {
            
            if id is NSNumber {
                self.id = id as? NSNumber
            } else {
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                self.id = numberFormatter.number(from: id as! String)
            }
        }
        
        if let title = jsonData["title"] {
            self.title = title as? String
        }
        
        if let excuse = jsonData["excuse"] {
            self.excuse = excuse as? String
        }
        if let startDate = jsonData["startDate"] {
            self.startDate = startDate as? String
        }
    }
    
    func icon() -> UIImage {
        
        var imageName = "MindsetIcon"
        
        if (self.category == "mindset") {
            imageName = "MindsetIcon"
        }
        else if (self.category == "health") {
            imageName = "HealthIcon"
        }
        else if (self.category == "family") {
            imageName = "FamilyIcon"
        }
        else if (self.category == "business") {
            imageName = "BusinessIcon"
        }

        return UIImage.init(named: imageName)!
    }
}
