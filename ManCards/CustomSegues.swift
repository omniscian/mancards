//
//  CustomSegues.swift
//  ManCards
//
//  Created by Ian Houghton on 22/07/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class FadeInPushSegue: UIStoryboardSegue {
    
    var animated: Bool = true
    
    override func perform() {
        
        let transition: CATransition = CATransition()
        transition.type = kCATransitionFade
        transition.duration = 0.2
        self.source.view.window?.layer.add(transition, forKey: "kCATransition")
        self.source.navigationController?.pushViewController(self.destination, animated: false)
    }
    
}

class FadeOutPopSegue: UIStoryboardSegue {
    
    override func perform() {
        
        let transition: CATransition = CATransition()
            
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
        
        self.source.view.window?.layer.add(transition, forKey: "kCATransition")
        self.source.navigationController?.popViewController(animated: false)
    }
    
}
