//
//  UserCollectionViewCell.swift
//  ManCards
//
//  Created by MacMini on 11/05/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class UserView: UIView {
 
    @IBOutlet private var view: UIView!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var currentCardLabel: UILabel!

    private var completionBlock: () -> Void = {}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        loadNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        loadNib()
    }
    
    func loadNib() {
        self.view = Bundle.main.loadNibNamed("UserView", owner: self, options: nil)?[0] as! UIView
        self.view.frame = bounds
        self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(self.view);
    }

    func configureForUser(user: User, completionBlock: @escaping () -> Void){
        self.nameLabel.text = user.fullName
        
        var cardDescription = "No current card!"
        
        if user.currentCard != nil {
            cardDescription = (user.currentCard?.cardDescription)!
        }
        
        self.currentCardLabel.text = cardDescription
        self.completionBlock = completionBlock
    }
    
    @IBAction func viewCardButtonPressed(){
        self.completionBlock()
    }
}
