//
//  ReviewViewController.swift
//  ManCards
//
//  Created by Ian Houghton on 16/05/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ReviewViewController: MCViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView : UITableView?
    var datasource = Array<Card>()
    var selectedTempCard: Card?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.toolbar.hideMenuButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getReviewCards()
    }
    
    func getReviewCards(){
        FirebaseManager.shared.fetchCardsToReview { (cards) in
            self.datasource = cards
            self.tableView?.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ReviewToReviewDetailSegue" {
            let reviewDetailVc = segue.destination as! ReviewDetailViewController
            reviewDetailVc.selectedCard = self.selectedTempCard
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cardCell")
        
        let card = self.datasource[indexPath.row]
        cell?.textLabel?.text = card.title
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        self.selectedTempCard = self.datasource[indexPath.row]
        
        self.performSegue(withIdentifier: "ReviewToReviewDetailSegue", sender: nil)
    }
}
