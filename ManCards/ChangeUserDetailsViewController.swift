//
//  ChangeUserDetailsViewController.swift
//  ManCards
//
//  Created by Ian Houghton on 05/10/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class ChangeUserDetailsViewController: MCViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet private var nameTextField: UITextField!{
        didSet{
            self.nameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        }
    }
    @IBOutlet private var userImageView: UIImageView!
    @IBOutlet private var confirmButton: UIButton!{
        didSet{
            self.confirmButton.isEnabled = false
        }
    }
    
    private var pickedImage: UIImage? {
        didSet{
            self.confirmButton.isEnabled = true
        }
    }
    private var user: User?
    private var imageUploadComplete: Bool = true
    private var nameUploadComplete: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.toolbar.hideMenuButton()
        self.nameTextField?.text = self.user?.fullName
        
        if self.user?.image != nil {
            self.userImageView.image = self.user?.image
        }
        else if self.user?.imageUrl != nil {
            self.userImageView.downloadedFrom(url: URL(string: (self.user?.imageUrl!)!)!, user: (self.user)!)
        }
        else {
            self.userImageView.setImage(string: self.user?.fullName, color: .gray)
        }

    }
    
    func configureForUser(user: User){
        self.user = user
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        self.confirmButton.isEnabled = textField.text == self.user?.fullName ? false : true
    }
    
    @IBAction func newPhotoButtonPressed(){
        let alertController = UIAlertController(title: "Please select:", message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Photo library", style: .default, handler: { (action) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.pickedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        dismiss(animated:true, completion: nil)
    }
    
    @IBAction func confirmButtonPressed(){
        self.confirmButton.showLoading()
        
        if self.nameTextField?.text != self.user?.fullName {
            self.nameUploadComplete = false
            FirebaseManager.shared.updateUserName(name: self.nameTextField.text, completionHandler: { (success) in
                self.nameUploadComplete = true
                if success{
                    if self.imageUploadComplete && self.nameUploadComplete {
                        self.confirmButton.hideLoading()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                else{
                    if self.imageUploadComplete && self.nameUploadComplete {
                        let alertController = UIAlertController(title: "Error", message: "There was an error with your upload, please try again.", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler:nil))
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            })
        }
        
        if self.pickedImage != nil {
            self.imageUploadComplete = false
            FirebaseManager.shared.updateUserImage(image: self.pickedImage, completionHandler: { (success) in
                self.imageUploadComplete = true
                if success{
                    if self.imageUploadComplete && self.nameUploadComplete {
                        self.confirmButton.hideLoading()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                else{
                    if self.imageUploadComplete && self.nameUploadComplete {
                        let alertController = UIAlertController(title: "Error", message: "There was an error with your upload, please try again.", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler:nil))
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            })
        }
        
    }

}
