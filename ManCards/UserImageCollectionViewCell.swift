//
//  UserImageCollectionViewCell.swift
//  ManCards
//
//  Created by Ian Houghton on 21/07/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class UserImageCollectionViewCell: UICollectionViewCell {
 
    @IBOutlet private var userImageView: UIImageView!
    
    func configure(user: User){
        if user.image != nil {
            self.userImageView.image = user.image
        }
        else if user.imageUrl != nil {
            self.userImageView.downloadedFrom(url: URL(string: user.imageUrl!)!, user: user)
        }
        else {
            self.userImageView.setImage(string: user.fullName, color: .gray)
        }
    }
    
}
