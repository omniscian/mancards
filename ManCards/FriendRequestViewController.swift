//
//  FriendRequestViewController.swift
//  ManCards
//
//  Created by Ian Houghton on 11/06/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit
import FirebaseDatabase

class FriendRequestViewController: MCViewController, UITableViewDelegate, UITableViewDataSource, FriendRequestTableViewCellDelegate {
    
    @IBOutlet var tableview : UITableView?
    
    var currentUser: User = FirebaseManager.shared.currentUser!
    var datasource = [User]()
    var allUsers: [User] = FirebaseManager.shared.users
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.toolbar.hideMenuButton()

        for uidDict in (self.currentUser.followRequest)!{
            
            for user in self.allUsers{
                if user.uid == uidDict.value {
                    self.datasource.append(user)
                }
            }
        }
        
        self.tableview?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FriendRequestTableViewCell
        
        let user = self.datasource[indexPath.row]

        cell.configure(user: user, delegate: self)
        
        return cell
    }
    
    func friendRequestAcceptedForUser(user: User) {
        FirebaseManager.shared.acceptFriendRequest(user: user)
        
        self.datasource.remove(at: self.datasource.index(of: user)!)
        self.tableview?.reloadData()
    }
    
    func friendRequestRejectedForUser(user: User) {
        FirebaseManager.shared.rejectFriendRequest(user: user)
        
        self.datasource.remove(at: self.datasource.index(of: user)!)
        self.tableview?.reloadData()
    }
}
