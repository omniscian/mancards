//
//  DemoViewController.swift
//  ManCards
//
//  Created by Ian Houghton on 14/08/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class DemoViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var pageControl: UIPageControl!{
        didSet{
            self.pageControl.numberOfPages = 5
        }
    }
    
    var demoImages: Array <String> = ["Demo1", "Demo2", "Demo3", "Demo4", "Demo5"]
    var demoText: Array <String> = ["Welcome to Man Cards, the app aimed at providing you with little tips and tricks to help you improve your quality of life and becoming a better you. Start by selecting a Man Card and making sure you complete it.",
                                    "The card view gives you more details about your Man Card, such as recommended completion time. We advise not to skip any cards so that you try different things and different approaches to reaching your goals. Some will work, some won't, but you won't know until you try!",
                                    "In the users section, you can find friends and keep up to date with how they're doing. Find more friends and inspire each other to keep growing.",
                                    "In the menu, you can create your own Man Card for specific goals you have, and these can be anything, as generic or as personalised as you want. This app is about you.",
                                    "Make sure you post to social media as much as possible for accountability. Post when you complete or fail a card, and explain why. Hold yourself responsible, take on a task and complete it.\n Become a better you!"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.setup()
    }
    
    func setup(){
        
        for view in self.scrollView.subviews{
            view.removeFromSuperview()
        }
        
        let pageSize = self.scrollView.frame.size
        var page: CGFloat = 0
    
        for i in 0 ... 4 {
            let view = DemoView(frame: CGRect(x: (pageSize.width * page), y: 0, width: pageSize.width, height: pageSize.height))
            view.configure(image: UIImage.init(named: self.demoImages[i])!, text: self.demoText[i])
            self.scrollView.addSubview(view)
            
            page = page + 1
        }
        
        self.scrollView.contentSize = CGSize(width: pageSize.width * 5, height: pageSize.height)
    }
    
    @IBAction func gotItButtonPressed(){
        UserDefaults.standard.set(true, forKey: "demoShown")
        self.dismiss(animated: true, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = self.scrollView.frame.size.width
        let fractionalPage = self.scrollView.contentOffset.x / pageWidth
        let page = lround(Double(fractionalPage))
        self.pageControl.currentPage = page
    }
}
