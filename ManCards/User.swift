//
//  User.swift
//  ManCards
//
//  Created by Ian Houghton on 09/05/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit
import UserNotifications

class User: NSObject {
    var admin: Bool?
    var fullName: String?
    var email: String?
    var uid: String?
    var following: [String : String]?
    var followers: [String : String]?
    var skippedCards: Array<Card>?
    var completedCards: Array<Card>?
    var failedCards: Array<Card>?
    var currentCard: Card?
    var preferences: Preferences?
    var followRequest: Dictionary<String, String>?
    var lastInteraction: String?
    var lastDate: String?
    var imageUrl: String?
    var image: UIImage?
    
    override init() {
        
    }
    
    convenience init(jsonData: [String : AnyObject]) {
        self.init()
        
        if let admin = jsonData["admin"] {
            self.admin = (admin as? NSNumber)?.boolValue
        }
        
        if let fullName = jsonData["fullName"] {
            self.fullName = fullName as? String
        }
        
        if let email = jsonData["email"] {
            self.email = email as? String
        }
        
        if let uid = jsonData["uid"] {
            self.uid = uid as? String
        }
        
        if let skippedCards = jsonData["skippedCards"] {
            
            var skippedCardsArray = Array<Card>()
            var unparsedSkippedCardsArray = Array<Any>()
            
            if let dictionary = skippedCards as? NSDictionary {
                unparsedSkippedCardsArray = dictionary.allValues as Array
            }
            else
            {
                unparsedSkippedCardsArray = (skippedCards as? Array)!
            }
            
            for i in 0...(unparsedSkippedCardsArray.count) - 1 {
                if let cardDict = unparsedSkippedCardsArray[i] as? [String : AnyObject]
                {
                    let skippedCard = Card(jsonData: cardDict)
                    skippedCardsArray.append(skippedCard)
                }
            }
            
            self.skippedCards = skippedCardsArray
        }
        
        if let completedCards = jsonData["completedCards"] {
            
            var completedCardsArray = Array<Card>()
            var unparsedCompletedCardsArray = Array<Any>()
            
            if let dictionary = completedCards as? NSDictionary {
                unparsedCompletedCardsArray = dictionary.allValues as Array
            }
            else
            {
                unparsedCompletedCardsArray = (completedCards as? Array)!
            }
            
            for i in 0...(unparsedCompletedCardsArray.count) - 1 {
                if let cardDict = unparsedCompletedCardsArray[i] as? [String : AnyObject]
                {
                    let skippedCard = Card(jsonData: cardDict)
                    completedCardsArray.append(skippedCard)
                }
            }
            
            self.completedCards = completedCardsArray
        }
        
        if let failedCards = jsonData["failedCards"] {
            
            var failedCardsArray = Array<Card>()
            var unparsedFailedCardsArray = Array<Any>()
            
            if let dictionary = failedCards as? NSDictionary {
                unparsedFailedCardsArray = dictionary.allValues as Array
            }
            else
            {
                unparsedFailedCardsArray = (failedCards as? Array)!
            }
            
            for i in 0...(unparsedFailedCardsArray.count) - 1 {
                if let cardDict = unparsedFailedCardsArray[i] as? [String : AnyObject]
                {
                    let skippedCard = Card(jsonData: cardDict)
                    failedCardsArray.append(skippedCard)
                }
            }
            
            self.failedCards = failedCardsArray
        }
        
        if let currentCards = jsonData["currentCards"] {
            
            var unparsedCurrentCardsArray = Array<Any>()
            
            if let dictionary = currentCards as? NSDictionary {
                unparsedCurrentCardsArray = dictionary.allValues as Array
            }
            else
            {
                unparsedCurrentCardsArray = (currentCards as? Array)!
            }
            
            for i in 0...(unparsedCurrentCardsArray.count) - 1 {
                if let cardDict = unparsedCurrentCardsArray[i] as? [String : AnyObject]
                {
                    let currentCard = Card(jsonData: cardDict)
                    self.currentCard = currentCard
                }
            }
        }
        
        if let following = jsonData["following"] {
            self.following = following as? [String : String]
        }
        
        if let followers = jsonData["followers"] {
            self.followers = followers as? [String : String]
        }
        
        if let preferences = jsonData["preferences"] {
            self.preferences = Preferences(jsonData: preferences as! [String : AnyObject])
        }
        
        if let followRequests = jsonData["followRequest"]{
            self.followRequest = followRequests as? [String : String]
        }
        
        if let lastInteraction = jsonData["lastInteraction"]{
            self.lastInteraction = lastInteraction as? String
        }
        
        if let lastDate = jsonData["lastDate"]{
            self.lastDate = lastDate as? String
        }
        
        if let imageUrl = jsonData["imageUrl"] {
            self.imageUrl = imageUrl as? String
        }
    }
    
    func doesCardMatchPreferences(card: Card) -> Bool {
        for category in (self.preferences?.categories!)!{
            if card.category == category {
                return true
            }
        }
        
        return false
    }
    
    func canUseCard(card: Card) -> Bool {
        
        var canUseCard = true
        
        if self.skippedCards != nil {
            for skippedCard in self.skippedCards!{
                if skippedCard.id == card.id {
                    canUseCard = false
                }
            }
        }
        
        if self.completedCards != nil {
            for completedCard in self.completedCards!{
                if completedCard.id == card.id {
                    canUseCard = false
                }
            }
        }
        
        if self.failedCards != nil {
            for failedCard in self.failedCards!{
                if failedCard.id == card.id {
                    canUseCard = false
                }
            }
        }
        
        if !(self.preferences?.categories?.contains(card.category!))! {
            canUseCard =  false
        }
        
        return canUseCard
    }
    
    func setLocalNotifications(){
        
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        
        if let card = self.currentCard {
            if card.startDate != nil{
                UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                    if settings.authorizationStatus == .authorized {
                        // Enabled
                        let content = UNMutableNotificationContent()
                        content.title = "Update progress"
                        content.body = "Let us know if you completed your Man Card!"
                        content.sound = UNNotificationSound.default()
                        
                        let date = Date(timeInterval: TimeInterval(60 * 60 * 24 * (card.duration?.intValue)!), since: (card.startDate?.toDate())!)
                        let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second,], from: date)
                        
                        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
                        
                        let identifier = "ManCardLocalNotification"
                        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
                        
                        UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
                            if let error = error {
                                print(error)
                            }
                        })
                    }
                }
                
            }
        }
    }
}
