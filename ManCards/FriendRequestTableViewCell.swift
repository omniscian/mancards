//
//  FriendRequestTableViewCell.swift
//  ManCards
//
//  Created by Ian Houghton on 13/06/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

protocol FriendRequestTableViewCellDelegate {
    func friendRequestAcceptedForUser(user: User)
    func friendRequestRejectedForUser(user: User)
}

class FriendRequestTableViewCell: UITableViewCell {

    @IBOutlet var nameLabel : UILabel?
    
    var user : User?
    var delegate : FriendRequestTableViewCellDelegate?
    
    func configure(user: User, delegate: FriendRequestTableViewCellDelegate){
        self.user = user
        self.nameLabel?.text = user.fullName
        self.delegate = delegate
    }

    @IBAction func acceptButtonPressed(sender: UIButton){
        self.delegate!.friendRequestAcceptedForUser(user: self.user!)
    }
    
    @IBAction func rejectButtonPressed(sender: UIButton){
        self.delegate!.friendRequestRejectedForUser(user: self.user!)
    }
}
