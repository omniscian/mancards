//
//  FirebaseManager.swift
//  ManCards
//
//  Created by MacMini on 10/05/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage

class FirebaseManager: NSObject {
    
    private override init() { }
    
    static let shared = FirebaseManager()

    private lazy var usersRef: DatabaseReference = Database.database().reference().child("users")
    private lazy var cardsRef: DatabaseReference = Database.database().reference().child("cards")
    private lazy var tempCardsRef: DatabaseReference = Database.database().reference().child("tempCards")
    
    var users: [User] = []
    var cards: [Card] = []
    var currentUser : User? = nil
    
    func login(email: String, password: String, completionHandler:@escaping (Bool) -> ()){
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
            if user != nil {
                
                UserDefaults.standard.set(email, forKey: "emailAddress")
                UserDefaults.standard.set(password, forKey: "password")
                
                completionHandler(true)
            }
            else{
                completionHandler(false)
            }
        })
    }
    
    func register(email: String, password: String, name: String, completionHandler:@escaping (Bool) -> ()){
        Auth.auth().createUser(withEmail:email, password:password, completion: { (user, error) in
            if let registeredUser = user {
                
                let ref = Database.database().reference(fromURL: "https://man-cards.firebaseio.com/")
                let usersRef = ref.child("users").child(registeredUser.uid)
                let userInfo : [String : Any] = ["email" : email, "fullName" : name, "uid" : registeredUser.uid]
                
                // persist logged in user into UserDefaults
                UserDefaults.standard.set(email, forKey: "emailAddress")
                UserDefaults.standard.set(password, forKey: "password")
                
                usersRef.updateChildValues(userInfo, withCompletionBlock: { (error, ref) in
                    if error == nil {
                        completionHandler(true)
                    }
                    else{
                        completionHandler(false)
                    }
                })
            }
            else{
                completionHandler(false)
            }
        })
    }
    
    func fetchCards(completionHandler:@escaping ([Card]) -> ()) {
        
        self.cards = [Card]()
        
        self.cardsRef.observe(.childAdded, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String : AnyObject]{
                let card = Card(jsonData: dictionary)
                self.cards.append(card)
            }
            
            DispatchQueue.main.async(execute: {
                completionHandler(self.cards)
            })
        })
    }
    
    func fetchUsers(completionHandler:@escaping (_ users: [User], _ currentUser: User) -> ()) {
    
        self.users = [User]()
        
        self.usersRef.observe(.value, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String : AnyObject]{
                
                self.users = [User]()
                
                for key in dictionary.keys {
                    
                    let userDict = dictionary[key] as? [String : AnyObject]
                    
                    let user = User(jsonData: userDict!)

                    if user.uid != Auth.auth().currentUser?.uid{
                        self.users.append(user)
                    }
                    else {
                        self.currentUser = user
                        self.currentUser?.setLocalNotifications()
                    }
                }
                
                DispatchQueue.main.async(execute: {
                    completionHandler(self.users, self.currentUser!)
                })
            }
        })
    }
    
    func fetchCardsToReview(completionHandler:@escaping (_ cards: [Card]) -> ()){
        
        var cards = [Card]()
        
        self.tempCardsRef.observe(.childAdded, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String : AnyObject]{
                let card = Card()
                card.setValuesForKeys(dictionary)
                cards.append(card)
            }
            
            DispatchQueue.main.async(execute: {
                completionHandler(cards)
            })
        })
    }
    
    func removeTempCard(tempCard: Card, completionHandler:@escaping (_ success: Bool) -> ()){
        
        let cardId: String = String(format: "%@", tempCard.id!)
        
        self.tempCardsRef.child(cardId).removeValue { (error, ref) in
            DispatchQueue.main.async(execute: {
                completionHandler(error == nil)
            })
        }
    }
    
    func updatePreferences(categories: Array<String>){
        let prefDict = ["categories" : categories]
        self.usersRef.child((self.currentUser?.uid)!).child("preferences").updateChildValues(prefDict as [AnyHashable : Any])
    }
    
    func updateUserName(name: String!, completionHandler:@escaping (Bool) -> ()){
        self.usersRef.child((self.currentUser?.uid)!).child("fullName").setValue(name){ (error, ref) in
            if error == nil {
                completionHandler(true)
            }
            else{
                completionHandler(false)
            }
        }
    }
    
    func updateUserImage(image: UIImage?, completionHandler:@escaping (Bool) -> ()){
        
        let imageName = NSUUID().uuidString
        let storageRef = Storage.storage().reference().child((self.currentUser?.uid)!).child("\(imageName).jpg")
        
        if let profileImage = image, let uploadData = UIImageJPEGRepresentation(profileImage, 0.1) {
            
            storageRef.putData(uploadData, metadata: nil, completion: { (metadata, err) in
                
                if err != nil {
                    print(err!)
                    completionHandler(false)
                    return
                }
                
                if let profileImageUrl = metadata?.downloadURL()?.absoluteString {
                    self.usersRef.child((self.currentUser?.uid)!).child("imageUrl").setValue(profileImageUrl){ (error, ref) in
                        if error == nil {
                            completionHandler(true)
                        }
                        else{
                            completionHandler(false)
                        }
                    }
                }
            })
        }
    }
    
    func failCard(excuse: String, currentUser: User, card: Card){
        self.usersRef.child(currentUser.uid!).child("currentCards").removeValue()
        
        let failedDict = NSMutableDictionary(dictionary: (card.dictionaryValue()))
        failedDict.setValue(excuse, forKeyPath: "excuse")
        
        self.usersRef.child(currentUser.uid!).child("failedCards").child((card.id?.stringValue)!).updateChildValues(failedDict as! [AnyHashable : Any])
        
        self.updateLastInteraction(lastInteraction: "Failed a Man Card! \"" + (card.title)! + "\"")
    }
    
    func acceptCard(card: Card){
        let cardDict = NSMutableDictionary(dictionary: (card.dictionaryValue()))
        cardDict.setValue(Date().toString(), forKeyPath: "startDate")
        
        self.usersRef.child((self.currentUser?.uid!)!).child("currentCards").child((card.id?.stringValue)!).updateChildValues(cardDict as! [AnyHashable : Any])
        
        self.updateLastInteraction(lastInteraction: "Accepted a new Man Card! \"" + (card.title)! + "\"")
    }
    
    func acceptCard(userInfo: [String : Any]){
        self.usersRef.child((self.currentUser?.uid!)!).child("currentCards").child(userInfo["id"] as! String).updateChildValues(userInfo)
        
        let title = userInfo["title"] as! String
        
        self.updateLastInteraction(lastInteraction: "Accepted a new Man Card! \"" + title + "\"")
    }
    
    func completeCard(currentUser: User, card: Card){
        self.usersRef.child(currentUser.uid!).child("currentCards").removeValue()
        self.usersRef.child(currentUser.uid!).child("completedCards").child((card.id?.stringValue)!).updateChildValues((card.dictionaryValue()))
        
        self.updateLastInteraction(lastInteraction: "Completed a Man Card! \"" + (card.title)! + "\"")
    }
    
    func updateLastInteraction(lastInteraction: String){
        self.usersRef.child((self.currentUser?.uid!)!).updateChildValues(["lastInteraction" : lastInteraction] as [AnyHashable : Any])
        
        let formatter = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        formatter.dateFormat = "dd/MM/yyyy HH:mm"
        formatter.timeZone = TimeZone(secondsFromGMT: (60 * 60))
        formatter.string(from: Date())
        
        self.usersRef.child((self.currentUser?.uid!)!).updateChildValues(["lastDate" : formatter.string(from: Date())] as [AnyHashable : Any])
    }
    
    func skipCard(currentUser: User, card: Card){
        self.usersRef.child(currentUser.uid!).child("skippedCards").child((card.id?.stringValue)!).updateChildValues((card.dictionaryValue()))
    }
    
    func removeSkippedCards(currentUser: User){
        self.usersRef.child(currentUser.uid!).child("skippedCards").observe(.childAdded, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String : AnyObject]{
                
                let card = Card()
                card.setValuesForKeys(dictionary)
                
                self.usersRef.child(currentUser.uid!).child("skippedCards").child((card.id?.stringValue)!).removeValue()
            }
        })
    }
    
    func removeFailedCards(currentUser: User){
        self.usersRef.child(currentUser.uid!).child("failedCards").observe(.childAdded, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String : AnyObject]{
                
                let card = Card()
                card.setValuesForKeys(dictionary)
                
                self.usersRef.child(currentUser.uid!).child("failedCards").child((card.id?.stringValue)!).removeValue()
            }
        })
    }
    
    func acceptFriendRequest(user: User) {
        let following = ["following/\(self.currentUser?.uid! ?? "")" : self.currentUser?.uid] as! [String : AnyHashable]
        let followers = ["followers/\(user.uid!)" : user.uid] as! [String : AnyHashable]
        
        self.usersRef.child(user.uid!).updateChildValues(following)
        self.usersRef.child((self.currentUser?.uid!)!).updateChildValues(followers)
        
        self.usersRef.child((self.currentUser?.uid!)!).child("followRequest/\(user.uid!)").removeValue()
        
        self.updateLastInteraction(lastInteraction: "Started following " + (self.currentUser?.fullName!)!)
    }
    
    func rejectFriendRequest(user: User) {
        self.usersRef.child((self.currentUser?.uid!)!).child("followRequest/\(user.uid!)").removeValue()
    }
    
    func removeFollower(user: User) {
        self.usersRef.child((self.currentUser?.uid!)!).child("following/\(user.uid!)").removeValue()
        self.usersRef.child(user.uid!).child("followers/\(self.currentUser?.uid! ?? "")").removeValue()
    }

    func removeFollowRequestPending (user: User) {
        self.usersRef.child(user.uid!).child("followRequest/\(self.currentUser?.uid! ?? "")").removeValue()
    }
    
    func follow(user: User) {
        let dict : [String : Any] = [(self.currentUser?.uid!)! : self.currentUser?.uid! as Any]
        let followRequest = ["followRequest" : dict]
        self.usersRef.child(user.uid!).updateChildValues(followRequest)
    }
    
    func addNewTempCard(desc: String, category: String, duration: String, title: String, completionHandler:@escaping (Bool, [String : Any]) -> ()){
    
        let id = NSNumber(value: arc4random_uniform(1000000))
        let userInfo : [String : Any] = ["id" : id.stringValue, "cardDescription" : desc, "category" : category, "duration" : NSNumber(value: Int32(duration)!), "title" : title]
        
        self.tempCardsRef.child(id.stringValue).updateChildValues(userInfo) { (error, ref) in
            if error == nil {
                completionHandler(true, userInfo)
            }
            else{
                completionHandler(false, userInfo)
            }
        }
    }
}


