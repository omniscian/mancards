//
//  CurrentCardView.swift
//  ManCards
//
//  Created by Ian Houghton on 19/07/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

protocol CurrentCardViewDelegate: class {
    func shouldShowCurrentCard()
}

class CurrentCardView: UIView {
    
    @IBOutlet private var iconImageView: UIImageView!
    @IBOutlet private var cardTitleLabel: UILabel!
    @IBOutlet private var cardDurationLabel: UILabel!
    @IBOutlet private var cardDescriptionLabel: UILabel!
    
    @IBOutlet private var cardHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var titleToImageConstraint: NSLayoutConstraint!
    
    var delegate: CurrentCardViewDelegate?
    private var currentCard: Card?
    
    func configure(card: Card){
        self.iconImageView.isHidden = false
        self.currentCard = card
        self.iconImageView.image = card.icon()
        self.cardTitleLabel.text = card.title
        self.cardDurationLabel.text = card.durationDisplayString
        self.cardDescriptionLabel.text = card.cardDescription
        
        self.titleToImageConstraint.constant = 10
        let height = card.description.height(withConstrainedWidth: self.cardDescriptionLabel.frame.width, font: self.cardDescriptionLabel.font)
        self.cardHeightConstraint.constant = height + self.cardDescriptionLabel.frame.origin.y + 20;
        
        UIView.animate(withDuration: 0.1) { 
            self.layoutIfNeeded()
        }
    }
    
    func configureForNoCurrentCard(){
        self.iconImageView.isHidden = true
        self.cardTitleLabel.text = "No current card"
        self.cardDurationLabel.text = nil
        self.cardDescriptionLabel.text = nil
        
        self.titleToImageConstraint.constant = -30
        self.cardHeightConstraint.constant = 100;
        
        UIView.animate(withDuration: 0.1) {
            self.layoutIfNeeded()
        }
    }
    
    @IBAction func viewCurrentCardPressed(){
        if ((self.currentCard) != nil){
            self.delegate?.shouldShowCurrentCard()
        }
    }
}
