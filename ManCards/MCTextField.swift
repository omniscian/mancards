//
//  MCTextField.swift
//  ManCards
//
//  Created by Ian Houghton on 19/07/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

@IBDesignable
class MCTextField: UITextField {
    @IBInspectable var insetX: CGFloat = 0
    @IBInspectable var insetY: CGFloat = 0
    
    // placeholder position
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
    
    // text position
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
}
