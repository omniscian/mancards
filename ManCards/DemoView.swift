//
//  DemoView.swift
//  ManCards
//
//  Created by Ian Houghton on 14/08/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class DemoView: UIView {

    @IBOutlet private var view: UIView!
    @IBOutlet private var textLabel: UILabel!
    @IBOutlet private var demoImageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        loadNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        loadNib()
    }
    
    func loadNib() {
        self.view = Bundle.main.loadNibNamed("DemoView", owner: self, options: nil)?[0] as! UIView
        self.view.frame = bounds
        self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(self.view);
    }
    
    func configure(image: UIImage, text: String){
        self.textLabel.text = text
        self.demoImageView.image = image
    }
}
