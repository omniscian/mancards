//
//  UsersPreviewViewController.swift
//  ManCards
//
//  Created by Ian Houghton on 20/07/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

protocol UsersPreviewViewControllerDelegate: class {
    func shouldShowUsersViewController()
}


class UsersPreviewViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet private var collectionView: UICollectionView!
    
    var delegate: UsersPreviewViewControllerDelegate?
    private var _users: [User]? = nil
    var users: [User]? {
        get {
            return self._users
        }
        set {
            self._users = newValue
            self.refresh()
        }
    }
    
    private func refresh(){
        self.collectionView.reloadData()
    }
    
    @IBAction private func chevronButtonPressed(){
        self.delegate?.shouldShowUsersViewController()
    }
    
    // MARK: - UICollectionViewDatasource
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 240, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if (self.users != nil){
            return (self.users?.count)!
        }
        else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userCell", for: indexPath) as? UserImageCollectionViewCell
        
        let user = self.users?[indexPath.row]
        cell?.configure(user: user!)
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.shouldShowUsersViewController()
    }
}
