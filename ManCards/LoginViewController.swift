//
//  ViewController.swift
//  ManCards
//
//  Created by Ian Houghton on 08/05/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FBSDKLoginKit

enum LoginScreenType: Int {
    case register = 0
    case login = 1
}

class LoginViewController: MCViewController, UITextFieldDelegate, MCSegmentedControlDelegate {

    private var screenType: LoginScreenType?
    @IBOutlet private var segmentedController : MCSegmentedControl!{
        didSet{
            self.segmentedController.configure(titleOne: "Register", titleTwo: "Sign in")
            self.segmentedController.delegate = self
        }
    }
    
    @IBOutlet private var usernameTextfield : UITextField!{
        didSet{
            if (self.screenType == .login) {
                self.configureForLogin(animated: false)
            }
            else{
                self.configureForRegister(animated: false)
            }
        }
    }
    
    @IBOutlet private var passwordTextfield : UITextField!
    @IBOutlet private var reenterPasswordLabel : UILabel!
    @IBOutlet private var reenterPasswordTextfield : UITextField!
    @IBOutlet private var nameLabel : UILabel!
    @IBOutlet private var nameTextfield : UITextField!
    @IBOutlet private var loginButton : UIButton!
    @IBOutlet private var registerButton : UIButton!
    @IBOutlet private var activityIndicator : UIActivityIndicatorView!{
        didSet{
            self.activityIndicator.stopAnimating()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.toolbar.hideMenuButton()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.segmentedController.setSelected(index: (self.screenType?.rawValue)!)
    }
    
    func configure(loginScreenType: LoginScreenType){
        self.screenType = loginScreenType
    }
    
    // MARK: - MCSegmentedControlDelegate
    
    internal func didSelectSegment(index: NSInteger, sender: MCSegmentedControl?){
        switch index {
        case 0:
            self.configureForRegister(animated: true)
            break
        case 1:
            self.configureForLogin(animated:true)
            break
        default:
            break
        }
        
        self.screenType = LoginScreenType(rawValue: index)
    }

    private func configureForLogin(animated: Bool){
        if (animated) {
            UIView.animate(withDuration: 0.2) {
                self.hideRegisterButtons()
            }
        }
        else {
            self.hideRegisterButtons()
        }
    }
    
    private func hideRegisterButtons(){
        self.reenterPasswordLabel.alpha = 0
        self.reenterPasswordTextfield.alpha = 0
        self.nameLabel.alpha = 0
        self.nameTextfield.alpha = 0
        self.loginButton.isHidden = false
        self.registerButton.isHidden = true
    }
    
    private func configureForRegister(animated: Bool){
        if (animated) {
            UIView.animate(withDuration: 0.2) {
                self.showRegisterButtons()
            }
        }
        else {
            self.showRegisterButtons()
        }
    }
    
    private func showRegisterButtons(){
        self.reenterPasswordLabel.alpha = 1
        self.reenterPasswordTextfield.alpha = 1
        self.nameLabel.alpha = 1
        self.nameTextfield.alpha = 1
        self.loginButton.isHidden = true
        self.registerButton.isHidden = false
    }
    
    private func login(email: String, password: String){
        FirebaseManager.shared.login(email: email, password: password) { (success) in
            
            self.activityIndicator.stopAnimating()
            self.loginButton.setTitle("Sign in", for: .normal)
            
            if (success){
                self.performSegue(withIdentifier: "LoginToDashboardSegue", sender: nil)
            }
            else {
                let alertController = UIAlertController(title: "Error", message: "Unable to login at this time. Please check your details and try again", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - UITextFieldDelegate
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField == self.usernameTextfield) {
            self.passwordTextfield.becomeFirstResponder()
        }
        else if (textField == self.passwordTextfield && self.screenType == .register) {
            self.reenterPasswordTextfield.becomeFirstResponder()
        }
        else if (textField == self.passwordTextfield) {
            self.passwordTextfield?.resignFirstResponder()
            self.loginButtonPressed(sender: nil)
        }
        else if (textField == self.reenterPasswordTextfield) {
            self.nameTextfield?.becomeFirstResponder()
        }
        else if (textField == self.nameTextfield) {
            self.nameTextfield?.resignFirstResponder()
            self.registerButtonPressed(sender: nil)
        }
        
        return true
    }
    
    // MARK: - IBActions
    
    @IBAction private func loginButtonPressed(sender: UIButton?){
        
        if let email = self.usernameTextfield?.text, let password = self.passwordTextfield?.text {
            self.activityIndicator.startAnimating()
            self.loginButton.setTitle("", for: .normal)
            self.login(email: email, password: password)
        }
    }
    
    @IBAction private func registerButtonPressed(sender: UIButton?){
        
        if self.passwordTextfield.text != self.reenterPasswordTextfield.text {
            let alertController = UIAlertController(title: "Error", message: "The passwords enter do no match. Please re-enter and try again", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        
        if let email = self.usernameTextfield?.text, let password = self.passwordTextfield?.text, let name = self.nameTextfield?.text {
            
            self.activityIndicator.startAnimating()
            self.registerButton.setTitle("", for: .normal)
            
            FirebaseManager.shared.register(email: email, password: password, name: name, completionHandler: { (success) in
                
                self.activityIndicator.stopAnimating()
                self.registerButton.setTitle("Register", for: .normal)
                
                if (success){
                    self.performSegue(withIdentifier: "LoginToDashboardSegue", sender: nil)
                }
                else {
                    let alertController = UIAlertController(title: "Error", message: "Unable to register at this time. Please check your details and try again", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                }
            })
        }
        else {
            let alertController = UIAlertController(title: "Error", message: "Please enter all fields and try again", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)

        }
    }
    
    @IBAction private func facebookLogin(sender: UIButton) {
        let fbLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
            if let error = error {
                print("Failed to login: \(error.localizedDescription)")
                return
            }
            
            guard let accessToken = FBSDKAccessToken.current() else {
                print("Failed to get access token")
                return
            }
            
            let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
            
            Auth.auth().signIn(with: credential, completion: { (user, error) in
                if let error = error {
                    print("Login error: \(error.localizedDescription)")
                    let alertController = UIAlertController(title: "Login Error", message: error.localizedDescription, preferredStyle: .alert)
                    let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(okayAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                    return
                }
                
                UserDefaults.standard.set(true, forKey: "facebookLogin")
                
                let ref = Database.database().reference(fromURL: "https://man-cards.firebaseio.com/")
                let usersRef = ref.child("users").child((Auth.auth().currentUser?.uid)!)
                let userInfo : [String : Any] = ["email" : Auth.auth().currentUser?.email as Any, "fullName" : Auth.auth().currentUser?.displayName! as Any, "uid" : Auth.auth().currentUser!.uid as Any]
                
                // Persist facebook login
                
                var userAdded = false
                
                Database.database().reference().child("users").observe(.childAdded, with: { (snapshot) in

                    if let dictionary = snapshot.value as? [String : AnyObject]{
                        let user = User(jsonData: dictionary)
                        
                        if user.uid == Auth.auth().currentUser?.uid{
                            userAdded = true
                        }
                        
                    }
                    
                    DispatchQueue.main.async(execute: {
                        if userAdded == false{
                            usersRef.updateChildValues(userInfo, withCompletionBlock: { (error, ref) in
                                if error == nil {
                                    self.performSegue(withIdentifier: "LoginToDashboardSegue", sender: nil)
                                }
                            })
                        }
                        else
                        {
                            self.performSegue(withIdentifier: "LoginToDashboardSegue", sender: nil)
                        }
                    })
                })
            })
        }   
    }
}

