//
//  FollowersViewController.swift
//  ManCards
//
//  Created by Ian Houghton on 20/07/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

protocol FollowersViewControllerDelegate: class {
    func showCard(card: Card?)
}

class FollowersViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet private var scrollView: UIScrollView!
    
    private var currentUser: User?
    private var users: [User]?
    
    weak var delegate: FollowersViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func setup(){
        let pageSize = self.scrollView.frame.size
        var page: CGFloat = 0
        
        self.users = self.users?.shuffled()
        
        if (self.currentUser?.following != nil){
            for userDict in (self.currentUser?.following!)! {
                let user = self.userForUid(uid: userDict.value)
                let view = UserView(frame: CGRect(x: (pageSize.width * page), y: 0, width: pageSize.width - 20, height: pageSize.height))
                view.configureForUser(user: user!, completionBlock: {
                    self.delegate?.showCard(card: user?.currentCard)
                })
                self.scrollView.addSubview(view)
                
                page = page + 1
            }
            
            self.scrollView.contentSize = CGSize(width: pageSize.width * CGFloat((self.users?.count)!), height: pageSize.height)
        }
    }
    
    func refresh(){
        self.currentUser = FirebaseManager.shared.currentUser
        self.users = FirebaseManager.shared.users
        
        for view in self.scrollView.subviews {
            view.removeFromSuperview()
        }
        
        self.setup()
    }
    
    private func userForUid(uid: String) -> User?{
        
        for user in self.users! {
            if user.uid == uid {
                return user
            }
        }
        
        return nil
    }
}
