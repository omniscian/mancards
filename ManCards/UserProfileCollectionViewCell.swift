//
//  UserTableViewCell.swift
//  ManCards
//
//  Created by Ian Houghton on 14/06/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class UserProfileCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var userImageView: UIImageView!
    @IBOutlet private var cardsCompletedLabel: UILabel!
    @IBOutlet private var followStateImageView: UIImageView!
    @IBOutlet private var iconBackgroundView: UIView!
    
    func configure(forUser: User, isFollowing: Bool, isPending: Bool){
        self.nameLabel?.text = forUser.fullName
        if forUser.image != nil {
            self.userImageView.image = forUser.image
        }
        else if forUser.imageUrl != nil {
            self.userImageView.downloadedFrom(url: URL(string: forUser.imageUrl!)!, user: forUser)
        }
        else {
            self.userImageView.setImage(string: forUser.fullName, color: .gray)
        }
        
        let cardsCount = "\(forUser.completedCards?.count ?? 0)"
        let cardText = forUser.completedCards?.count == 1 ? " card" : " cards"
        
        self.cardsCompletedLabel.text = cardsCount + cardText + " complete"
        
        if isFollowing {
            self.iconBackgroundView.backgroundColor = .mcLightGrey
            self.followStateImageView.image = UIImage.init(named: "FriendsIcon")
        }
        else if isPending {
            self.iconBackgroundView.backgroundColor = .mcDarkGrey
            self.followStateImageView.image = UIImage.init(named: "RequestedIcon")
        }
        else {
            self.iconBackgroundView.backgroundColor = .mcDarkGrey
            self.followStateImageView.image = UIImage.init(named: "AddFriendIcon")
        }
    }
}
