//
//  MenuViewController.swift
//  ManCards
//
//  Created by Ian Houghton on 27/07/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet private var tableview: UITableView!
    
    private var datasource: Array<String> = ["Add a Man Card"]
    private var currentUser: User? = FirebaseManager.shared.currentUser
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (self.currentUser?.followRequest != nil){
            if ((self.currentUser?.followRequest?.count)! > 0){
                self.datasource.append("Friend requests")
            }
        }
        
        if (self.currentUser?.admin != nil){
            if (self.currentUser?.admin)! {
                self.datasource.append("Review Man Cards")
            }
        }
        self.datasource.append("Change user details")
        self.datasource.append("How to")
        self.datasource.append("Log out")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.currentUser = FirebaseManager.shared.currentUser
        
        self.tableview.reloadData()
    }
    
    private func logout(){
        let alertController = UIAlertController(title: "Logout", message: "Are you sure you want to log out?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            
            UserDefaults.standard.set(nil, forKey: "emailAddress")
            UserDefaults.standard.set(nil, forKey: "password")
            
            self.performSegue(withIdentifier: "UnwindToLandingPageSegue", sender: nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell") as? MenuTableViewCell
        
        cell?.menuTitleLabel?.text = self.datasource[indexPath.row]
        
        if (self.datasource[indexPath.row] == "Friend requests") {
            if (self.currentUser?.followRequest != nil){
                if ((self.currentUser?.followRequest?.count)! > 0){
                    cell?.notificationImageView.setImage(string: "\(self.currentUser?.followRequest?.count ?? 0)", color: .red, circular: true)
                }
            }
            else {
                cell?.notificationImageView.image = nil
            }
        }
        else{
            cell?.notificationImageView.image = nil
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableview.deselectRow(at: indexPath, animated: true)
        self.performAction(forIndex: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MenuToChangeDetailsSegue"{
            let vc = segue.destination as! ChangeUserDetailsViewController
            vc.configureForUser(user: self.currentUser!)
        }
    }
    
    private func performAction(forIndex: Int){
        
        let selectedString = self.datasource[forIndex]
        
        switch selectedString {
        case "Add a Man Card":
            self.performSegue(withIdentifier: "MenuToAddNewCardSegue", sender: nil)
            break
        case "Friend requests":
            self.performSegue(withIdentifier: "MenuToFriendRequestSegue", sender: nil)
            break
        case "Review Man Cards":
            self.performSegue(withIdentifier: "MenuToReviewSegue", sender: nil)
            break
        case "How to":
            self.performSegue(withIdentifier: "MenuToDemoSegue", sender: nil)
            break
        case "Log out":
            self.logout()
            break
        case "Change user details":
            self.performSegue(withIdentifier: "MenuToChangeDetailsSegue", sender: nil)
            break
        default:
            break
        }
    }
    
    @IBAction private func closeButtonPressed(){
        self.dismiss(animated: true, completion: nil)
    }

}
