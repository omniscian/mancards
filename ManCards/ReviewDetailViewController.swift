//
//  ReviewDetailViewController.swift
//  ManCards
//
//  Created by Ian Houghton on 16/05/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class ReviewDetailViewController: MCViewController {

    @IBOutlet var titleTextField: UITextField?
    @IBOutlet var descTextView: UITextView?
    @IBOutlet var categoryTextField: UITextField?
    @IBOutlet var durationTextField: UITextField?
    var selectedCard : Card? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleTextField?.text = self.selectedCard?.title
        self.descTextView?.text = self.selectedCard?.cardDescription
        self.categoryTextField?.text = self.selectedCard?.category
        self.durationTextField?.text = self.selectedCard?.duration?.stringValue
    }
    
    func deleteFromTempCards(){
        FirebaseManager.shared.removeTempCard(tempCard: self.selectedCard!) { (success) in
            self.navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func acceptButtonPressed(sender: UIButton){
        // Move from temp into new DB
    
        let ref = Database.database().reference()

        self.deleteFromTempCards()
        
        // Get number of cards to work out ID
        var id = 0
        Database.database().reference().child("cards").observe(.childAdded, with: { (snapshot) in
            if (snapshot.value as? [String : AnyObject]) != nil{
                id += 1
            }
            
            DispatchQueue.main.async(execute: {
                id += 1
                let userInfo : [String : Any] = ["cardDescription" : self.descTextView?.text! as Any, "category" : self.categoryTextField?.text! as Any, "duration" : NSNumber(value: Int32((self.durationTextField?.text)!)!), "title" : self.titleTextField?.text! as Any, "id" : id]
                
                ref.child("cards").child(String(id)).updateChildValues(userInfo, withCompletionBlock: { (error, ref) in
                    if error == nil {
                        self.navigationController?.popViewController(animated: true)
                    }
                    else {
                        let alertController = UIAlertController(title: "Error", message: "Request failed. Try again.", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                })
            })
        })
    }
    
    @IBAction func rejectButtonPressed(sender: UIButton){
        self.deleteFromTempCards()
    }
}
