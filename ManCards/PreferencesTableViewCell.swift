//
//  PreferencesTableViewCell.swift
//  ManCards
//
//  Created by Ian Houghton on 02/08/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class PreferencesTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var tickImageView: UIImageView!
    @IBOutlet var seperatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let backgroundView = UIView(frame: self.frame)
        backgroundView.backgroundColor = .clear
        self.selectedBackgroundView = backgroundView
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        self.tickImageView.isHidden = !self.isSelected
        
        self.selectedBackgroundView?.backgroundColor = .clear
        self.backgroundColor = .clear
        self.seperatorView.backgroundColor = .mcLightGrey
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        self.seperatorView.backgroundColor = .mcLightGrey
    }
}
