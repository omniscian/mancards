//
//  MCToolBar.swift
//  ManCards
//
//  Created by Ian Houghton on 19/07/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

protocol MCToolBarDelegate: class {
    func backButtonPressed()
    func menuButtonPressed()
}

class MCToolBar: UIView {

    @IBOutlet private var view: UIView!
    @IBOutlet private var backButton: UIButton!
    @IBOutlet private var menuButton: UIButton!
    @IBOutlet private var notificationImageView: UIImageView!

    var delegate: MCToolBarDelegate?
    private var currentUser: User?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.view = Bundle.main.loadNibNamed("MCToolBar", owner: self, options: nil)?[0] as! UIView
        self.addSubview(self.view)
        self.view.translatesAutoresizingMaskIntoConstraints = false
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: ["view":self.view]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: [], metrics: nil, views: ["view":self.view]))
        
        FirebaseManager.shared.fetchUsers { (users, currentUser) in
            self.currentUser = currentUser
            
            if (self.currentUser?.followRequest != nil){
                if ((self.currentUser?.followRequest?.count)! > 0){
                    self.notificationImageView.setImage(string: "\(self.currentUser?.followRequest?.count ?? 0)", color: .red, circular: true)
                }
            }
            else {
                self.notificationImageView.image = nil
            }
        }
    }
    
    func hideBackButton(){
        self.backButton.isHidden = true
    }
    
    func hideMenuButton(){
        self.menuButton.isHidden = true
        self.notificationImageView.isHidden = true
    }
    
    @IBAction private func backButtonPressed(){
        self.delegate?.backButtonPressed()
    }
    
    @IBAction private func menuButtonPressed(){
        self.delegate?.menuButtonPressed()
    }
}
