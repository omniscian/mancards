//
//  CardViewController.swift
//  ManCards
//
//  Created by MacMini on 10/05/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit
import Social
    
protocol CardViewControllerDelegate: class {
    func didSelectNewCard(sender: CardViewController)
    func closeButtonPressed()
    func changeCard()
    func showPreferences()
    func showAddYourOwn()
}

enum InteractionType: Int {
    case accepted = 0
    case completed = 1
}

class CardViewController: UIViewController {

    @IBOutlet private var cardImageView : UIImageView!
    @IBOutlet private var cardTitleLabel : UILabel!
    @IBOutlet private var cardDescriptionLabel : UITextView!
    @IBOutlet private var cardDurationLabel : UILabel!
    @IBOutlet private var cardCategoryLabel : UILabel!
    @IBOutlet private var skipLabel : UILabel!
    
    @IBOutlet private var skipButton : UIButton!
    @IBOutlet private var acceptButton : UIButton!
    
    @IBOutlet private var completedButton : UIButton!
    @IBOutlet private var quitButton : UIButton!
    
    @IBOutlet private var acceptedView : UIView!
    @IBOutlet private var descriptionView : UIView!

    weak private var delegate: CardViewControllerDelegate?
    
    var cards = [Card]()
    var currentCard: Card?
    var currentUser: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cardDescriptionLabel?.indicatorStyle = .white
    
        if self.currentUser?.currentCard != nil || self.currentCard != nil{
            
            if (self.currentCard != nil){
                self.skipButton?.isHidden = true
                self.acceptButton?.isHidden = true
                self.skipLabel?.isHidden = true
                self.completedButton?.isHidden = true
                self.quitButton?.isHidden = true
            }
            else{
                self.currentCard = self.currentUser?.currentCard
                
                self.skipButton?.isHidden = true
                self.acceptButton?.isHidden = true
                self.skipLabel?.isHidden = true
                self.completedButton?.isHidden = false
                self.quitButton?.isHidden = false
            }
            
            self.setScreenForCurrentCard()
        }
        else
        {
            self.chooseAndDisplayRandomCard()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.cardDescriptionLabel.setContentOffset(.zero, animated: false)
    }
    
    func configureWithCards(cards: [Card], currentUser: User, delegate: CardViewControllerDelegate){
        
        self.currentUser = currentUser
        self.delegate = delegate
        
        for card in cards {
            if (self.currentUser?.preferences?.categories?.contains(card.category!))! {
                self.cards.append(card)
            }
        }
    }
    
    func showUserCard(card: Card, delegate: CardViewControllerDelegate){
        self.currentCard = card
        self.delegate = delegate
    }
    
    func chooseAndDisplayRandomCard(){
        
        self.acceptedView?.isHidden = true
        
        var total = 0
        
        if self.currentUser?.failedCards != nil {
            total += (self.currentUser?.failedCards?.count)!
        }
        
        if self.currentUser?.skippedCards != nil {
            total += (self.currentUser?.skippedCards?.count)!
        }
        
        if self.currentUser?.completedCards != nil {
            total += (self.currentUser?.completedCards?.count)!
        }
        
        if (total == self.cards.count){
            let alertController = UIAlertController(title: "No more cards", message: "You've exhausted our card collection so you're either keen or found loads you don't fancy. Why not look over your skipped or failed cards and push yourself out your comfort zone? You can also change your preferences to open up more options, or write your own card!", preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: "View skipped", style: .default, handler: { (success) in
                self.currentUser?.skippedCards?.removeAll()
                
                FirebaseManager.shared.removeSkippedCards(currentUser: self.currentUser!)
            }))
            
            alertController.addAction(UIAlertAction(title: "View failed", style: .default, handler: { (success) in
                self.currentUser?.failedCards?.removeAll()
                
                FirebaseManager.shared.removeFailedCards(currentUser: self.currentUser!)
            }))
            
            alertController.addAction(UIAlertAction(title: "Preferences", style: .default, handler: { (success) in
                self.delegate?.showPreferences()
            }))
            
            alertController.addAction(UIAlertAction(title: "Add my own", style: .default, handler: { (success) in
                self.delegate?.showAddYourOwn()
            }))
            
            self.present(alertController, animated: true, completion: nil)
            
            self.delegate?.closeButtonPressed()
        }
        else{
            let randomIndex = Int(arc4random_uniform(UInt32(self.cards.count)))
            self.currentCard = self.cards[randomIndex]
            
            if !(self.currentUser?.canUseCard(card: self.currentCard!))! {
                self.chooseAndDisplayRandomCard()
            }
            
            self.setScreenForCurrentCard()
        }
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    
    func setScreenForCurrentCard(){
        self.cardDescriptionLabel.isScrollEnabled = false
        self.cardImageView.image = self.currentCard?.icon()
        self.cardTitleLabel?.text = self.currentCard?.title
        self.cardDescriptionLabel?.text = self.currentCard?.cardDescription
        self.cardDescriptionLabel.isScrollEnabled = true
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
        
        if CGFloat((self.cardDescriptionLabel?.contentSize.height)!) > CGFloat((self.cardDescriptionLabel?.frame.size.height)!) {
            self.cardDescriptionLabel?.flashScrollIndicators()
        }
        
        self.cardDurationLabel?.text = self.currentCard?.durationDisplayString
        self.cardCategoryLabel?.text = "Category: " + (self.currentCard?.category?.capitalized)!
    }
    
    func shareContent(interactionType: InteractionType){
        
        let originalFrame = self.cardDescriptionLabel.frame
        let fixedWidth = self.cardDescriptionLabel?.frame.size.width
        self.cardDescriptionLabel?.sizeThatFits(CGSize(width: fixedWidth!, height: CGFloat.greatestFiniteMagnitude))
        let newSize = self.cardDescriptionLabel?.sizeThatFits(CGSize(width: fixedWidth!, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = self.cardDescriptionLabel?.frame
        newFrame?.size = CGSize(width: max((newSize?.width)!, fixedWidth!), height: (newSize?.height)!)
        self.cardDescriptionLabel?.frame = newFrame!;
        
        self.view.layoutIfNeeded()
        
        let uploadView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (self.descriptionView?.frame.size.height)! + (self.acceptedView?.frame.size.height)! + 50))
        uploadView.backgroundColor = .black
        self.acceptedView?.isHidden = false
        uploadView.addSubview(self.acceptedView!)
        let copiedView: UIView = self.descriptionView.copyView()
        uploadView.addSubview(copiedView)
        copiedView.frame = CGRect(x: 0, y: (self.acceptedView?.frame.size.height)!, width: (copiedView.frame.size.width), height: (copiedView.frame.size.height))
        
        let image = UIImage(view: uploadView)
        var text = ""
        
        if interactionType == .accepted {
            text = String(format: "Accepted a new Man Card: %@", (self.currentCard?.cardDescription)!)
        }
        else if interactionType == .completed {
            text = String(format: "Completed a Man Card: %@", (self.currentCard?.cardDescription)!)
        }
        
        self.cardDescriptionLabel.frame = originalFrame
        
        let activityViewController = UIActivityViewController(activityItems: [image, text], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        activityViewController.excludedActivityTypes = [.airDrop, .postToWeibo, .addToReadingList, .assignToContact, .copyToPasteboard, .openInIBooks, .postToFlickr, .postToVimeo, .postToTencentWeibo, .postToWeibo]
        
        activityViewController.completionWithItemsHandler = {(activity, success, items, error) in
            if (interactionType == .accepted) {
                self.closeButtonPressed(sender: nil)
            }
            else {
                self.delegate?.changeCard()
                self.chooseAndDisplayRandomCard()
                self.skipButton?.isHidden = false
                self.acceptButton?.isHidden = false
                self.skipLabel?.isHidden = false
                
                self.completedButton?.isHidden = true
                self.quitButton?.isHidden = true
            }
        };
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func postToFacebook(interactionType: InteractionType){
        let vc = SLComposeViewController(forServiceType:SLServiceTypeFacebook)

        // resize description view
        let fixedWidth = self.cardDescriptionLabel?.frame.size.width
        self.cardDescriptionLabel?.sizeThatFits(CGSize(width: fixedWidth!, height: CGFloat.greatestFiniteMagnitude))
        let newSize = self.cardDescriptionLabel?.sizeThatFits(CGSize(width: fixedWidth!, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = self.cardDescriptionLabel?.frame
        newFrame?.size = CGSize(width: max((newSize?.width)!, fixedWidth!), height: (newSize?.height)!)
        self.cardDescriptionLabel?.frame = newFrame!;
        
        self.view.layoutIfNeeded()
        
        let uploadView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (self.descriptionView?.frame.size.height)! + (self.acceptedView?.frame.size.height)!))
        uploadView.backgroundColor = .black
        self.acceptedView?.isHidden = false
        uploadView.addSubview(self.acceptedView!)
        uploadView.addSubview(self.descriptionView!)
        self.descriptionView?.frame = CGRect(x: 0, y: (self.acceptedView?.frame.size.height)!, width: (self.descriptionView?.frame.size.width)!, height: (self.descriptionView?.frame.size.height)!)
        vc?.add(UIImage(view: uploadView))
        
        if interactionType == .accepted {
            vc?.title = "Accepted a new Man Card"
            vc?.setInitialText(String(format: "Accepted a new Man Card: %@", (self.currentCard?.cardDescription)!))
        }
        else if interactionType == .completed {
            vc?.title = "Completed a Man Card!"
            vc?.setInitialText(String(format: "Completed a Man Card: %@", (self.currentCard?.cardDescription)!))
        }
        
        self.present(vc!, animated: true, completion: nil)
    }
    
    @IBAction func closeButtonPressed(sender: UIButton?){
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        
        self.delegate?.closeButtonPressed()
    }
    
    @IBAction func skipCardButtonPressed(sender: UIButton?){
        
        FirebaseManager.shared.skipCard(currentUser: self.currentUser!, card: self.currentCard!)
        
        if self.currentUser?.skippedCards == nil {
            self.currentUser?.skippedCards = [Card]();
        }
        
        self.currentUser?.skippedCards?.append(self.currentCard!)
        
        self.delegate?.changeCard()
        
        self.chooseAndDisplayRandomCard()
    }
    
    @IBAction func acceptCardButtonPressed(sender: UIButton?){
        
        FirebaseManager.shared.acceptCard(card: self.currentCard!)
        
        self.delegate?.didSelectNewCard(sender: self)
        self.shareContent(interactionType: .accepted)
    }
    
    @IBAction func completedButtonPressed(sender: UIButton?){
        FirebaseManager.shared.completeCard(currentUser: self.currentUser!, card: self.currentCard!)
        self.currentUser?.completedCards?.append(self.currentCard!)
        self.shareContent(interactionType: .completed)
    }
    
    @IBAction func quitButtonPressed(sender: UIButton?){
        
        let alertController = UIAlertController(title: "Reason", message: "Please enter a reason as to why you couldn't complete your Man Card:", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
            alert -> Void in
            
            let firstTextField = alertController.textFields![0] as UITextField
            
            FirebaseManager.shared.failCard(excuse: firstTextField.text!, currentUser: self.currentUser!, card: self.currentCard!)
            
            self.currentUser?.failedCards?.append(self.currentCard!)
            
            self.skipButton?.isHidden = false
            self.acceptButton?.isHidden = false
            self.skipLabel?.isHidden = false
            
            self.completedButton?.isHidden = true
            self.quitButton?.isHidden = true
            
            self.delegate?.changeCard()
            
            self.chooseAndDisplayRandomCard()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Reason"
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
