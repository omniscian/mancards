//
//  DashboardViewController.swift
//  ManCards
//
//  Created by Ian Houghton on 08/05/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit
import GoogleMobileAds

public extension LazyMapCollection  {
    
    func toArray() -> [Element]{
        return Array(self)
    }
}

class DashboardViewController: MCViewController, CardViewControllerDelegate, UsersPreviewViewControllerDelegate, CurrentCardViewDelegate, GADBannerViewDelegate, FollowersViewControllerDelegate {

    @IBOutlet private var currentCardView : CurrentCardView!{
        didSet{
            self.currentCardView.delegate = self
        }
    }
    @IBOutlet private var seperatorTopConstraint : NSLayoutConstraint!
    @IBOutlet private var userContainerView : UIView!
    @IBOutlet private var scrollView : UIScrollView!{
        didSet{
            self.scrollView.alpha = 0
        }
    }
    @IBOutlet private var activityIndicator : UIActivityIndicatorView!{
        didSet{
            self.activityIndicator.stopAnimating()
        }
    }
    @IBOutlet private var cardButton : UIButton! {
        didSet{
            self.cardButton?.isEnabled = false
        }
    }
    @IBOutlet private var cardContainerView: UIView!{
        didSet{
            self.cardContainerView.isHidden = true
        }
    }
    
    @IBOutlet private var bannerHolderView: UIView!
    @IBOutlet private var bannerViewHeightConstraint: NSLayoutConstraint!
    
    private var bannerView: GADBannerView?
    private var followersViewController: FollowersViewController?
    private var userPreviewViewController: UsersPreviewViewController?
    private var cards = [Card]()
    private var users = [User]()
    private var currentUser : User?
    
    private var fetchUsersComplete = false
    private var fetchCardsComplete = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        self.bannerView?.delegate = self
        self.bannerHolderView.addSubview(self.bannerView!)
        self.bannerView?.adUnitID = "ca-app-pub-9284667006743183/9116574135"
        self.bannerView?.rootViewController = self
        
        let request = GADRequest()
        request.testDevices = [ kGADSimulatorID];
        
        self.bannerView?.load(request)
        
        self.toolbar.hideBackButton()

        self.activityIndicator.startAnimating()
        self.refresh()
        
        // if not seen demo, show it
        let demoShown = UserDefaults.standard.bool(forKey: "demoShown")
        
        if (!demoShown) {
            self.performSegue(withIdentifier: "DashboardToDemoSegue", sender: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.refresh()
        self.bannerViewHeightConstraint.constant = 0
    }
    
    private func refresh(){
        self.fetchCards()
        self.fetchUsers()
    }
    
    private func refreshCurrentCardView(){
        if ((self.currentUser?.currentCard) != nil){
            self.currentCardView.configure(card: (self.currentUser?.currentCard)!)
        }
        else {
            self.currentCardView.configureForNoCurrentCard()
        }
    }
    
    override func viewDidLayoutSubviews() {
        
        let gap = (self.view.frame.height - 90) - (self.userContainerView.frame.origin.y + self.userContainerView.frame.height + 20)
        
        if (gap > 15) {
            self.seperatorTopConstraint.constant = gap
        }
    }
    
    func fetchCards(){
        
        FirebaseManager.shared.fetchCards { (cards) in
            self.cards = cards
            self.cardButton?.isEnabled = true
            self.fetchCardsComplete = true
            if (self.fetchUsersComplete){
                UIView.animate(withDuration: 0.2, animations: {
                    self.scrollView.alpha = 1
                }, completion: { (finished) in
                    self.activityIndicator.stopAnimating()
                })
            }
        }
    }
    
    func fetchUsers(){
        
        FirebaseManager.shared.fetchUsers { (users, currentUser) in
            self.users = users
            self.currentUser = currentUser
            
            self.refreshCurrentCardView()
            
            self.followersViewController?.refresh()
            self.userPreviewViewController?.users = self.users
            
            self.fetchUsersComplete = true
            if (self.fetchCardsComplete){
                UIView.animate(withDuration: 0.2, animations: {
                    self.scrollView.alpha = 1
                }, completion: { (finished) in
                    self.activityIndicator.stopAnimating()
                })
            }
            
            if self.currentUser?.preferences == nil {
                self.performSegue(withIdentifier: "DashboardToPreferences", sender: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "DashboardToFriendRequestSegue"){
            let controller = segue.destination as! FriendRequestViewController
            controller.currentUser = currentUser!
            controller.allUsers = self.users
        }
        else if (segue.identifier == "EmbedFollowersViewController"){
            self.followersViewController = segue.destination as? FollowersViewController
            self.followersViewController?.delegate = self
        }
        else if (segue.identifier == "EmbedUsersPreviewViewController"){
            self.userPreviewViewController = segue.destination as? UsersPreviewViewController
            self.userPreviewViewController?.delegate = self
        }
    }
    
    @IBAction func newCardButtonPressed(){
        
        if (self.currentUser?.currentCard != nil){
            let alertController = UIAlertController(title: "Error", message: "You already have a card in play. Do you want to fail this card and accept another?", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alertController.addAction(UIAlertAction(title: "Select new card", style: .default, handler: { (action) in
                let alertController = UIAlertController(title: "Reason", message: "Please enter a reason as to why you couldn't complete your Man Card:", preferredStyle: .alert)
                
                let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
                    alert -> Void in
                    
                    let firstTextField = alertController.textFields![0] as UITextField
                    
                    FirebaseManager.shared.failCard(excuse: firstTextField.text!, currentUser: self.currentUser!, card: (self.currentUser?.currentCard!)!)
                    
                    self.currentUser?.failedCards?.append((self.currentUser?.currentCard!)!)
                    self.currentUser?.currentCard = nil
                    
                    self.showCardViewController(card: nil)
                })
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                    (action : UIAlertAction!) -> Void in
                    
                })
                
                alertController.addTextField { (textField : UITextField!) -> Void in
                    textField.placeholder = "Reason"
                }
                
                alertController.addAction(cancelAction)
                alertController.addAction(saveAction)
                
                self.present(alertController, animated: true, completion: nil)
                })
            )
            
            self.present(alertController, animated: true, completion: nil)
        }
        else{
            self.showCardViewController(card: nil)
        }
    }
    
    func showCardViewController(card: Card?){
        self.cardContainerView.isHidden = false
        
        let controller = storyboard!.instantiateViewController(withIdentifier: "CardViewController") as! CardViewController
        
        if (card != nil) {
            controller.showUserCard(card: card!, delegate: self)
        }
        else{
            controller.configureWithCards(cards: self.cards, currentUser: self.currentUser!, delegate: self)
        }
        
        self.addChildViewController(controller)
        
        controller.view.frame = CGRect(x: 20, y: 20, width: self.view.frame.size.width - 40, height: self.view.frame.size.height - 40)
        controller.didMove(toParentViewController: self)
        controller.view.layoutIfNeeded()
        
        UIView.transition(with: self.cardContainerView, duration: 0.3, options: .transitionFlipFromRight, animations: {
            self.cardContainerView.addSubview(controller.view)
        }, completion: nil)
    }
    
    func shouldShowUsersViewController() {
        self.performSegue(withIdentifier: "DashboardToUsersSegue", sender: nil)
    }
    
    func didSelectNewCard(sender: CardViewController) {
        self.fetchUsers()
    }
    
    func closeButtonPressed() {
        UIView.transition(with: self.cardContainerView, duration: 0.5, options: .transitionFlipFromLeft, animations: {
            self.cardContainerView.isHidden = true
        }, completion:nil)
        
        self.refreshCurrentCardView()
    }
    
    func changeCard() {
        UIView.transition(with: self.cardContainerView, duration: 0.5, options: .transitionFlipFromRight, animations: nil, completion:nil)
    }
    
    func showPreferences() {
        self.performSegue(withIdentifier: "DashboardToPreferences", sender: nil)
    }
    
    func showAddYourOwn() {
        self.performSegue(withIdentifier: "DashboardToNewCardSegue", sender: nil)
    }
    
    func shouldShowCurrentCard(){
        self.showCardViewController(card: nil)
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        UIView.animate(withDuration: 1, animations: {
            self.bannerViewHeightConstraint.constant = 50
        })
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
    }
    
    func showCard(card: Card?) {
        if (card != nil) {
            self.showCardViewController(card: card)
        }
    }
}
