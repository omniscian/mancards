//
//  PreferencesViewController.swift
//  ManCards
//
//  Created by Ian Houghton on 20/05/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class PreferencesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView : UITableView?
    
    private var datasource = Array<String>()
    private var selectedIndeces = Array<Int>()
    private var selectedCategories = Array<String>()
    private var currentUser: User = FirebaseManager.shared.currentUser!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let alertController = UIAlertController.init(title: "Setup", message: "In order to get the most from this app, please answer a few questions to tailor the app to your needs", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
        
        self.datasource = ["Mindset", "Health", "Family", "Business", "Getting out of comfort zone"]
        
        for i in (0..<self.datasource.count){
            self.selectedIndeces.append(i)
        }
        
        self.tableView?.reloadData()
    }

    @IBAction func finishedButtonPressed(sender: UIButton){
        
        if self.selectedIndeces.count > 0 {
            
            for index in self.selectedIndeces{
                self.selectedCategories.append(self.datasource[index].lowercased())
            }
            
            FirebaseManager.shared.updatePreferences(categories: self.selectedCategories)
            
            self.dismiss(animated: true, completion: nil)
        }
        else{
            let alertController = UIAlertController.init(title: "Error", message: "Please select at least one category you want to improve on", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datasource.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (self.selectedIndeces.contains(indexPath.row)) {
            cell.setSelected(true, animated: true)
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "prefCell") as? PreferencesTableViewCell
        
        cell?.titleLabel?.text = self.datasource[indexPath.row]
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.selectedIndeces.contains(indexPath.row) {
            self.selectedIndeces.remove(at: self.selectedIndeces.index(of: indexPath.row)!)
        }
        else{
            self.selectedIndeces.append(indexPath.row)
        }
        
        tableView.cellForRow(at: indexPath)?.setSelected(true, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
    }
}
