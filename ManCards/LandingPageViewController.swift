//
//  LandingPageViewController.swift
//  ManCards
//
//  Created by Ian Houghton on 18/07/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FirebaseAuth

class LandingPageViewController: UIViewController {

    private var screenType: LoginScreenType = .login
    @IBOutlet private var getStartedButton: UIButton!
    @IBOutlet private var signInButton: UIButton!
    @IBOutlet private var activityIndicator: UIActivityIndicatorView!{
        didSet{
            self.activityIndicator.stopAnimating()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let email = UserDefaults.standard.value(forKey: "emailAddress"),let password = UserDefaults.standard.value(forKey: "password"){
            
            self.getStartedButton.isHidden = true
            self.signInButton.isHidden = true
            self.activityIndicator.startAnimating()
            
            FirebaseManager.shared.login(email: email as! String, password: password as! String) { (success) in
                
                self.activityIndicator.stopAnimating()
                
                if (success){
                    self.performSegue(withIdentifier: "LandingPageToDashboardViewController", sender: nil)
                }
                else {
                    let alertController = UIAlertController(title: "Error", message: "Unable to login at this time. Please check your details and try again", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
        else if UserDefaults.standard.bool(forKey: "facebookLogin"){
            guard let accessToken = FBSDKAccessToken.current() else {
                print("Failed to get access token")
                return
            }
            
            let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
            
            self.getStartedButton.isHidden = true
            self.signInButton.isHidden = true
            self.activityIndicator.startAnimating()
            
            Auth.auth().signIn(with: credential, completion: { (user, error) in
                
                self.activityIndicator.stopAnimating()
                
                if let error = error {
                    print("Login error: \(error.localizedDescription)")
                    let alertController = UIAlertController(title: "Login Error", message: error.localizedDescription, preferredStyle: .alert)
                    let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(okayAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                    return
                }
            
                self.performSegue(withIdentifier: "LandingPageToDashboardViewController", sender: nil)
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getStartedButton.isHidden = self.activityIndicator.isAnimating
        self.signInButton.isHidden = self.activityIndicator.isAnimating
    }
    
    // MARK: - IBActions
    @IBAction func unwindToLandingPage(segue:UIStoryboardSegue) {
        
    }
    
    @IBAction func getStartedButtonPressed(){
        self.screenType = .register
        self.performSegue(withIdentifier: "LandingPageToSignInViewController", sender: nil)
    }
    
    @IBAction func signInButtonPressed(){
        self.screenType = .login
        self.performSegue(withIdentifier: "LandingPageToSignInViewController", sender: nil)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let loginViewController = segue.destination as? LoginViewController
        loginViewController?.configure(loginScreenType: self.screenType)
    }
}
