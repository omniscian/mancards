//
//  MCViewController.swift
//  ManCards
//
//  Created by Ian Houghton on 19/07/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class MCViewController: UIViewController, MCToolBarDelegate {
    
    @IBOutlet var toolbar : MCToolBar!{
        didSet{
            self.toolbar.delegate = self
        }
    }
    
    // MARK: - MCToolBarDelegate
    
    func backButtonPressed() {
        let transition = CATransition()
        transition.duration = 0.2
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        self.navigationController?.popViewController(animated: false)
    }
    
    func menuButtonPressed() {
        
        let menuViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuNavController") as? UINavigationController
        self.present(menuViewController!, animated: true, completion: nil)
    }
}
