//
//  AddNewCardViewController.swift
//  ManCards
//
//  Created by MacMini on 12/05/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import M13Checkbox
import DropDown

class AddNewCardViewController: MCViewController, UITextFieldDelegate {

    private var categoryDropdown: DropDown = DropDown()
    private var durationDropdown: DropDown = DropDown()
    
    @IBOutlet private var titleTextField: UITextField?
    @IBOutlet private var descTextView: UITextView!{
        didSet{
            self.descTextView.textContainerInset = UIEdgeInsets(top: 8, left: 10, bottom: 0, right: 10);
        }
    }
    @IBOutlet private var durationTextField: UITextField!{
        didSet{
            self.durationDropdown.anchorView = self.durationTextField
            self.durationDropdown.dataSource = ["1", "7", "30", "90", "180"]
            self.durationDropdown.selectionAction = {(index: Int, item: String) in
                self.durationTextField?.text = item
            }
        }
    }
    @IBOutlet private var categoryTextField: UITextField!{
        didSet{
            self.categoryDropdown.anchorView = self.categoryTextField
            self.categoryDropdown.dataSource = ["Mindset", "Health", "Business", "Family"]
            self.categoryDropdown.selectionAction = {(index: Int, item: String) in
                self.categoryTextField?.text = item
            }
        }
    }
    
    @IBOutlet private var currentCardCheckbox: M13Checkbox! {
        didSet{
            self.currentCardCheckbox?.boxType = .square
            self.currentCardCheckbox?.animationDuration = 0.1
            self.currentCardCheckbox?.stateChangeAnimation = .bounce(.stroke)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.toolbar.hideMenuButton()
    }
    
    internal func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        if (textField == self.categoryTextField) {
            self.categoryDropdown.show()
            return false
        }
        
        if (textField == self.durationTextField) {
            self.durationDropdown.show()
            return false
        }

        return true
    }

    @IBAction private func submitButtonPressed(sender: UIButton?){
        
        if let title = self.titleTextField?.text, let desc = self.descTextView?.text, let duration = self.durationTextField?.text, let category = self.categoryTextField?.text, !title.isEmpty, !desc.isEmpty, !duration.isEmpty, !category.isEmpty {
            
            FirebaseManager.shared.addNewTempCard(desc: desc, category: category, duration: duration, title: title, completionHandler: { (success, userInfo) in
            
                if (success){
                    
                    var message = "Your card will be available to everyone when it has been approved by a moderator. "
                    
                    if (self.currentCardCheckbox.checkState == .checked){
                        FirebaseManager.shared.acceptCard(userInfo: userInfo)
                        
                        message.append("It has now been set as your current card.")
                    }
                    
                    let alertController = UIAlertController(title: "Card added", message: message, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            })
        }
        else{
            // show alert
            let alertController = UIAlertController(title: "Error", message: "All fields need to contain data", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler:nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
