//
//  MenuTableViewCell.swift
//  ManCards
//
//  Created by Ian Houghton on 15/08/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet var menuTitleLabel: UILabel!
    @IBOutlet var notificationImageView: UIImageView!
}
