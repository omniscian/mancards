//
//  UsersViewController.swift
//  ManCards
//
//  Created by Ian Houghton on 09/05/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class UsersViewController: MCViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {

    @IBOutlet private var collectionView : UICollectionView!
    @IBOutlet private var searchBar : UISearchBar!
    
    @IBOutlet private var leadingConstraint : NSLayoutConstraint!
    @IBOutlet private var trailingConstraint : NSLayoutConstraint!
    
    private var currentUser : User = FirebaseManager.shared.currentUser!
    private var followRequests : [String : AnyHashable]?
    private var datasource = [User]()
    private var filteredDatasource = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.leadingConstraint.constant = 0
        self.trailingConstraint.constant = 0
        
        if UIScreen.main.bounds.size.width == 375 {
            self.leadingConstraint.constant = 12
            self.trailingConstraint.constant = 12
        }
        else if UIScreen.main.bounds.size.width == 414 {
            self.leadingConstraint.constant = 31
            self.trailingConstraint.constant = 31
        }
        
        self.refresh()
    }
    
    func refresh(){
        FirebaseManager.shared.fetchUsers() { (users, currentUser) in
            self.datasource = users
            self.filteredDatasource = self.datasource
            self.collectionView?.reloadData()
        }
    }
    
    func isFollowing(user: User) -> Bool{
        if user.followers != nil {
            for uidDict in user.followers!{
                if uidDict.value == self.currentUser.uid{
                    return true
                }
            }
        }
        
        return false
    }
    
    func isRequestPending(user: User) -> Bool {
        
        if user.followRequest != nil {
            for uidDict in user.followRequest!{
                if uidDict.value == self.currentUser.uid{
                    return true
                }
            }
        }
        
        return false
    }
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var width = 117
        
        if UIScreen.main.bounds.size.width == 320 {
            width = 106
        }
        
        return CGSize(width: width, height: 194)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredDatasource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userCell", for: indexPath) as? UserProfileCollectionViewCell
        
        let user = self.filteredDatasource[indexPath.row]
        
        cell?.configure(forUser: user, isFollowing: self.isFollowing(user: user), isPending: self.isRequestPending(user: user))
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.collectionView.deselectItem(at: indexPath, animated: false)
        
        let user = self.filteredDatasource[indexPath.row]
        
        if self.isFollowing(user: user) {
            FirebaseManager.shared.removeFollower(user: user)
        }
        else if self.isRequestPending(user: user){
            FirebaseManager.shared.removeFollowRequestPending(user: user)
        }
        else {
            FirebaseManager.shared.follow(user: user)
        }
        
        self.refresh()
    }

    // MARK: - UISearchBarDelegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.characters.count == 0{
            self.filteredDatasource = self.datasource
            self.collectionView?.reloadData()
            return
        }
        
        var filtered = [User]()
        let searchTerm = searchText.lowercased()
        
        for user in self.datasource {
            let name = user.fullName?.lowercased()
            
            if (name?.contains(searchTerm))! {
                filtered.append(user)
            }
        }
        
        self.filteredDatasource = filtered
        self.collectionView?.reloadData()
    }
}
