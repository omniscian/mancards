//
//  MCSegmentedControl.swift
//  ManCards
//
//  Created by Ian Houghton on 19/07/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

protocol MCSegmentedControlDelegate: class {
    func didSelectSegment(index: NSInteger, sender: MCSegmentedControl?)
}

class MCSegmentedControl: UIView {

    var delegate: MCSegmentedControlDelegate?
    
    @IBOutlet private var barLeadingConstraint: NSLayoutConstraint!
    @IBOutlet private var barTrailingConstraint: NSLayoutConstraint!
    @IBOutlet private var view: UIView!
    @IBOutlet private var firstButton: UIButton!{
        didSet{
            self.firstButton.layer.borderColor = UIColor.darkGray.cgColor
            self.firstButton.isSelected = true
        }
    }
    
    @IBOutlet private var secondButton: UIButton!{
        didSet{
            self.secondButton.layer.borderColor = UIColor.darkGray.cgColor
            self.secondButton.isSelected = false
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.view = Bundle.main.loadNibNamed("MCSegmentedControl", owner: self, options: nil)?[0] as! UIView
        self.addSubview(self.view)
        self.view.translatesAutoresizingMaskIntoConstraints = false
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: ["view":self.view]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: [], metrics: nil, views: ["view":self.view]))
    }
    
    private func setBar(){
        
        self.layoutIfNeeded()
        
        if (self.firstButton.isSelected) {
            self.barLeadingConstraint.constant = 0;
            self.barTrailingConstraint.constant = self.firstButton.bounds.size.width
        }
        else {
            self.barLeadingConstraint.constant = self.secondButton.bounds.size.width
            self.barTrailingConstraint.constant = 0;
        }
        
        UIView.animate(withDuration: 0.1) {
            self.layoutIfNeeded()
        }
    }
    
    func configure(titleOne: String, titleTwo: String){
        self.firstButton.setTitle(titleOne, for: .normal)
        self.secondButton.setTitle(titleTwo, for: .normal)
    }
    
    func setSelected(index: Int){
        if (index == 0) {
            self.firstButton.isSelected = true
            self.secondButton.isSelected = false
            self.setBar()
        }
        else{
            self.secondButton.isSelected = true
            self.firstButton.isSelected = false
            self.setBar()
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func firstButtonPressed(){
        self.firstButton.isSelected = true
        self.secondButton.isSelected = false
        
        self.delegate?.didSelectSegment(index: 0, sender: self)
        
        self.setBar()
    }
    
    @IBAction func secondButtonPressed(){
        self.secondButton.isSelected = true
        self.firstButton.isSelected = false
        
        self.delegate?.didSelectSegment(index: 1, sender: self)
        
        self.setBar()
    }
}
