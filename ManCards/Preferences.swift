//
//  Preferences.swift
//  ManCards
//
//  Created by Ian Houghton on 21/05/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class Preferences: NSObject {
    var categories: Array<String>?
    
    override init() {
        
    }
    
    convenience init(jsonData: [String : AnyObject]) {
        self.init()
        
        if let categories = jsonData["categories"] {
            self.categories = categories as? Array<String>
        }
    }
}
